# -*- coding: utf-8 -*-
# @Author: ypm

import os
from django.core.mail import EmailMultiAlternatives

from mysite.settings import EMAIL_HOST_USER, CONFIRM_DAYS

os.environ['DJANGO_SETTINGS_MODULE'] = 'mysite.settings'

def send_mail(email, code):

    subject = '来自www.yupengmin.com的测试邮件'
    text_content = '欢迎访问www.yupengmin.com，这里是ypm的博客和教程站点，专注于Python和Django技术的分享！'
    html_content = '''
                        <p>感谢注册<a href="http://{}/confirm/?code={}" target=blank>www.liujiangblog.com</a>，\
                        这里是刘江的博客和教程站点，专注于Python、Django和机器学习技术的分享！</p>
                        <p>请点击站点链接完成注册确认！</p>
                        <p>此链接有效期为{}天！</p>
                        '''.format('127.0.0.1:8000', code, CONFIRM_DAYS)
    msg = EmailMultiAlternatives(subject, text_content, EMAIL_HOST_USER, [email])
    msg.attach_alternative(html_content, "text/html")
    msg.send()
