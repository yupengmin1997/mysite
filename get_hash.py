list1 = [1,2,3,4,5,6,7,8,9]

def search(value,list):
    """二分查找"""
    low = 0
    high = len(list)-1
    count = 1
    while low<=high:
        midpoint = (low+high)//2
        if list[midpoint] == value:
            return "一共查找【%d】次，下标为【%d】"%(count,midpoint)
        elif list[midpoint] > value:
            high = midpoint - 1
        else:
            low = midpoint + 1
        count += 1
    return "一共查找【%d】次，未找到值【%d】"%(count,value)

print(search(10,list1))
print(search(9,list1))
print(search(8,list1))
print(search(5,list1))