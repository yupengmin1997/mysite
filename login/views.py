import datetime

from django.shortcuts import render, redirect

# Create your views here.
from mysite.settings import CONFIRM_DAYS
from send_mail import send_mail
from login import forms
from login.models import User, ConfirmString
import hashlib
import jsonpickle


def index(request):
    if not request.session.get('is_login', None):
        return redirect('/login/')
    return render(request, 'login/index.html')


def login(request):
    if request.session.get('is_login', None):  # 不允许重复登录
        return redirect('/index/')
    if request.method == 'POST':
        login_form = forms.UserForm(request.POST)
        msg = '用户名或密码不为空！！！'
        if login_form.is_valid():
            username = login_form.cleaned_data.get('username')
            password = login_form.cleaned_data.get('password')

            try:
                user = User.objects.get(name=username)
            except:
                msg = '用户不存在'
                return render(request, 'login/login.html', locals())
            if user.password == password:
                request.session['is_login'] = True
                request.session['user_id'] = user.id
                request.session['user_name'] = user.name
                return redirect('/index/')
            else:
                msg = '密码不正确'
                return render(request, 'login/login.html', locals())
        else:
            return render(request, 'login/login.html', locals())
    login_form = forms.UserForm()
    return render(request, 'login/login.html', locals())


def logout(request):
    if not request.session.get('is_login', None):
        return redirect('/login/')
    request.session.flush()
    return redirect('/login/')


# 注册
def register(request):
    if request.session.get('is_login', None):
        return redirect('/index/')

    if request.method == 'POST':
        register_form = forms.RegisterForm(request.POST)
        msg = '用户名或密码为空'
        if register_form.is_valid():
            username = register_form.cleaned_data.get('username')
            password1 = register_form.cleaned_data.get('password1')
            password2 = register_form.cleaned_data.get('password2')
            email = register_form.cleaned_data.get('email')
            sex = register_form.cleaned_data.get('sex')

            if password1 != password2:
                msg = '两次输入密码不一致'
                return render(request, 'login/register.html', locals())
            else:
                same_user_name = User.objects.filter(name=username)
                if same_user_name:
                    msg = '该用户已存在'
                    return render(request, 'login/register.html', locals())
                same_user_email = User.objects.filter(email=email)
                if same_user_email:
                    msg = '该邮箱已被注册'
                    return render(request, 'login/register.html', locals())

                User.objects.create(name=username, password=hash(password1), email=email, sex=sex)
                user = User.objects.all().first()
                print("-------------", user)
                # 邮箱注册码
                code = make_confirm_string(user)
                # 发送邮件
                send_mail(email, code)

                msg = '请前往邮箱进行确认'
                return render(request, 'login/confirm.html', locals())

        else:
            return render(request, 'login/register.html', locals())

    register_form = forms.RegisterForm()
    return render(request, 'login/register.html', locals())


def make_confirm_string(user):
    user = jsonpickle.dumps(user)
    # 当前时间
    now = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    # 获取code编码
    code = hashlib.md5(now.encode('utf-8'))
    code.update(user.encode('utf-8'))
    code = code.hexdigest()
    user = jsonpickle.loads(user)
    ConfirmString.objects.create(code=code, user=user)
    return code


def user_confirm(request):
    # 获取注册码
    code = request.GET.get('code','')
    msg = ''
    try:
        confirm = ConfirmString.objects.filter(code=code)
    except:
        msg = '无效确认请求'
        return render(request,'login/confirm.html',locals())

    # 获取有效时间
    correct_time = confirm.c_time
    current_time = datetime.datetime.now()

    # 超过有效时间
    if current_time > correct_time + datetime.timedelta(CONFIRM_DAYS):
        message = '您的邮件已经过期！请重新注册!'
        confirm.user.delete()
        return render(request,'login/confirm.html',locals())

    else:
        confirm.user.has_confirmed = True
        confirm.user.save()
        confirm.delete()
        msg = '感谢确认，请使用账户登录！'
        return render(request,'login/confirm.html',locals())